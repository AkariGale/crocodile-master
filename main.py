import json
import os
import pandas as pd
import requests
import time
import traceback
import urllib
from backend import Backend
from datetime import datetime, timedelta

class Crocodile:

    token = os.environ['TOKEN']
    url = f"https://api.telegram.org/bot{token}/"
    db = None

    def __init__(self):

        self.db = Backend()

    def build_keyboard(self, items):

        keyboard = [[{"text": text, "callback_data": json.dumps(data)}] for text, data in items.items()]
        reply_markup = {"inline_keyboard": keyboard}

        return json.dumps(reply_markup)

    def get_url(self, url):

        response = requests.get(url)
        content = response.content.decode("utf8")

        return content

    def get_json_from_url(self, url):

        content = self.get_url(url)
        js = json.loads(content)

        if not js['ok']: print(js)
        return js

    def get_updates(self, offset=False):

        offset = f'&offset={offset}' if offset else ''
        url = f"{self.url}getUpdates?timeout=100{offset}"
        js = self.get_json_from_url(url)

        return js

    def get_last_update_id(self, updates):

        update_ids = []
        for update in updates["result"]:
            update_ids.append(int(update["update_id"]))

        return max(update_ids)

    def send_message(self, text, chat_id, reply_markup=None):

        text = urllib.parse.quote_plus(text)
        reply_markup = f"&reply_markup={reply_markup}" if reply_markup else ""
        url = f"{self.url}sendMessage?text={text}&chat_id={chat_id}&parse_mode=Markdown{reply_markup}"
        self.get_json_from_url(url)

    def answer_callback_query(self, query_id, text):

        text = urllib.parse.quote_plus(text)
        url = f"{self.url}answerCallbackQuery?callback_query_id={query_id}&text={text}&show_alert=true"
        self.get_json_from_url(url)

    def send_puzzle(self, chat_id, user):

        puzzle = self.db.start_puzzle(chat_id, user['id'])
        stamp = int(datetime.now().timestamp())
        master_data_show = {"uid": user["id"], "pid": puzzle["id"], "st": stamp, "t": 0}
        master_data_next = {"uid": user["id"], "pid": puzzle["id"], "st": stamp, "t": 1}
        keyboard = self.build_keyboard({"Cлово": master_data_show, "Следующее": master_data_next})
        self.send_message(f"Слово загадывает {user['first_name']} {user['last_name']}",
                            chat_id, reply_markup=keyboard)

        return puzzle

    def handle_message(self, message, puzzle):

        text = message["text"]
        user = message["from"]
        chat_id = message["chat"]["id"]
        if text == "/start":
            self.send_message(f"В каком чате будем играть?", chat_id)
        elif text == '/play':
            if puzzle and datetime.now() - puzzle["start_stamp"] < timedelta(0):
                self.send_message(f"Новое слово можно загадать через 10 минут", chat_id)
            else:
                return self.send_puzzle(chat_id, user)
        elif puzzle and user["id"] != puzzle["master_id"] and puzzle['word'] in text.lower():
            self.db.complete_puzzle(puzzle["id"], user["id"])
            stamp = int(datetime.now().timestamp())
            guessed_data = {"uid": user["id"], "pid": puzzle["id"], "st": stamp, "t": 2}
            keyboard = self.build_keyboard({"Хочу загадать слово!": guessed_data})
            self.send_message(f"{user['first_name']} {user['last_name']} угадал слово *{puzzle['word']}*!",
                                chat_id, reply_markup=keyboard)

        return None

    def handle_callback_query(self, callback_query, puzzle):

        user = callback_query["from"]
        data = json.loads(callback_query["data"])
        data['st'] = datetime.utcfromtimestamp(data['st'])
        chat_id = callback_query["message"]["chat"]["id"]
        if puzzle and data["t"] in [0, 1]:
            if puzzle["id"] == data["pid"]:
                if user["id"] == puzzle["master_id"]:
                    if data["t"] == 0:
                        self.answer_callback_query(callback_query["id"], puzzle["word"])
                    if data["t"] == 1:
                        if puzzle['changes'] > 0:
                            self.db.complete_puzzle(puzzle["id"])
                            puzzle = self.db.start_puzzle(chat_id, user["id"], puzzle['changes'] - 1)
                            self.answer_callback_query(callback_query["id"],
                                                        f"{puzzle['word']}, замен осталось: {puzzle['changes']}")
                            return puzzle
                        else:
                            self.answer_callback_query(callback_query["id"], f"Замены будут ближе к следующему раунду")
                else:
                    self.answer_callback_query(callback_query["id"], "Только для мастера этого раунда")
            else:
                self.answer_callback_query(callback_query["id"], "Это старый раунд")
        elif data["t"] == 2:
            if data["uid"] == user["id"] or datetime.now() - data['st'] > timedelta(seconds=10):
                puzzle = self.send_puzzle(chat_id, user)
                self.answer_callback_query(callback_query["id"], puzzle["word"])
                return puzzle
            else:
                self.answer_callback_query(callback_query["id"], "У отгадавшего приоритет в первые 10 секунд")

        return None

    def handle_updates(self, updates):

        puzzles = {}
        for update in updates["result"]:
            try:
                if "callback_query" in update:
                    chat_id = update["callback_query"]["message"]["chat"]["id"]
                else:
                    chat_id = update["message"]["chat"]["id"]
                if puzzles.get(chat_id, None) is None: puzzles[chat_id] = self.db.get_active_puzzle(chat_id)
                if "callback_query" in update:
                    response = self.handle_callback_query(update["callback_query"], puzzles[chat_id])
                else:
                    response = self.handle_message(update["message"], puzzles[chat_id])
                if response: puzzles[chat_id] = response
            except Exception as e:
                traceback.print_exc()


def main():

    croco = Crocodile()
    last_update_id = None
    while True:
        print(f"getting updates: {pd.Timestamp.now()}")
        updates = croco.get_updates(last_update_id)
        if len(updates["result"]) > 0:
            last_update_id = croco.get_last_update_id(updates) + 1
            croco.handle_updates(updates)
        time.sleep(0.5)


if __name__ == "__main__":
    main()
