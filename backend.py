import pandas as pd
import random
import sqlite3
from contextlib import contextmanager
from datetime import datetime
from dateutil import parser

class Backend:

    word_size = None

    def __init__(self, dbname="crocodile.sqlite"):

        self.dbname = dbname
        self.conn = sqlite3.connect(dbname)
        self.word_size = self.setup()

    @contextmanager
    def query(self, q, params=()):

        cur = self.conn.cursor()
        try:
            cur.execute(q, params)
            yield cur
        finally:
            cur.close()

    def setup(self):

        with open('database_definition.sql', 'r') as sql_file:
            sql_script = sql_file.read()

        cursor = self.conn.cursor()
        cursor.executescript(sql_script)
        self.conn.commit()

        stmt = "SELECT COUNT(*) FROM word"
        res = self.conn.execute(stmt).fetchall()[0][0]
        if res == 0:
            words = pd.read_csv("words")
            words = words.loc[(words.type == "noun") & (words.frequency <= 100), 'word']
            words.to_sql('word', self.conn, if_exists='append', index=False)
            res = self.conn.execute(stmt).fetchall()[0][0]

        return res

    def get_random_word(self):

        stmt = "SELECT word FROM word WHERE word_id = (?)"
        args = (random.randint(1, self.word_size), )
        word = self.conn.execute(stmt, args).fetchall()[0][0]

        return word

    def get_active_puzzle(self, chat_id):

        stmt = "SELECT puzzle_id, master_id, start_stamp, word FROM puzzle WHERE chat_id = (?) AND end_stamp IS NULL"
        args = (chat_id, )
        res = self.conn.execute(stmt, args).fetchall()
        res = {"id": res[0][0], "master_id": res[0][1], "start_stamp": parser.parse(res[0][2]),
                "word": res[0][3], "changes": 3} if res else None

        return res

    def start_puzzle(self, chat_id, master_id, changes=3):

        word = self.get_random_word()
        stmt = "INSERT INTO puzzle (chat_id, master_id, start_stamp, word) VALUES (?, ?, ?, ?)"
        args = (chat_id, master_id, datetime.now(), word)
        with self.query(stmt, args) as cur:
            puzzle_id = cur.lastrowid
        self.conn.commit()
        res = {"id": puzzle_id, "word": word, "changes": changes}

        return res

    def complete_puzzle(self, puzzle_id, guessed_id=None):

        stmt = "UPDATE puzzle SET guessed_id = (?), end_stamp = (?) WHERE puzzle_id = (?)"
        args = (guessed_id, datetime.now(), puzzle_id)
        self.conn.execute(stmt, args)
        self.conn.commit()
