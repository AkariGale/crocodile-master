CREATE TABLE IF NOT EXISTS puzzle (puzzle_id integer primary key,
                                    chat_id integer,
                                    master_id integer,
                                    guessed_id integer DEFAULT NULL,
                                    start_stamp timestamp,
                                    end_stamp timestamp DEFAULT NULL,
                                    word text);
CREATE INDEX IF NOT EXISTS puzzle_game_idx ON puzzle (chat_id);

CREATE TABLE IF NOT EXISTS result (chat_id integer,
                                    user_id integer,
                                    result integer);
CREATE INDEX IF NOT EXISTS result_game_idx ON result (chat_id);
CREATE INDEX IF NOT EXISTS result_user_idx ON result (user_id);

CREATE TABLE IF NOT EXISTS word (word_id integer primary key,
                                    word text);
